import * as PIXI from "pixi.js";

const particles = require('pixi-particles');

import Stats from 'stats.js';

const GAME_WIDTH = 1280;
const GAME_HEIGHT = 720;


const app = new PIXI.Application({
    antialias: true,
    forceFXAA: true,
    resolution: 1,
    width: GAME_WIDTH,
    height: GAME_HEIGHT
});
app.renderer.autoResize = true;
app.renderer.view.style.position = "relative";
app.renderer.view.style.display = "block";
app.renderer.view.style.margin = "0 auto";

document.getElementById("webgl").appendChild(app.view);


// let background: any;
const headerFontStyle = {
    fontFamily: 'Arial',
    fontSize: 28,
    fill: 0xFFFFFF,
    align: 'center',
    dropShadow: true,
    dropShadowAngle: 1,
    dropShadowBlur: 1,
    dropShadowDistance: 1
};

const textFontStyle = {
    fontFamily: 'Arial',
    fontSize: 24,
    fill: 0xFFFFFF,
    align: 'center',
    dropShadow: true,
    dropShadowAngle: 1,
    dropShadowBlur: 1,
    dropShadowDistance: 1
};

const header = new PIXI.Text('SOFTGAMES DEMO - Fire Particle Effect', headerFontStyle);
header.position.x = GAME_WIDTH / 2 - header.width / 2;
header.position.y = 20;
app.stage.addChild(header);

let spriteNumber = 0;
const description = 'Number of sprites on screen: ';

const positionText = new PIXI.Text(description + spriteNumber, textFontStyle);
positionText.position.x = GAME_WIDTH / 2 - positionText.width / 2;
positionText.position.y = header.height + 25;
app.stage.addChild(positionText);


const TextureAtlas_01 = "../../texture-packed-assets/texture.json";

// @ts-ignore
PIXI.Loader.shared.add(TextureAtlas_01).load(setup);
onWindowResize();

function setup() {

    const stats = new Stats();
    document.body.appendChild(stats.dom);

    // @ts-ignore
    const textureSheet_1 = PIXI.Loader.shared.resources[TextureAtlas_01].spritesheet;


    const particleContainer = new PIXI.Container();
    // const fire = new PIXI.Sprite(textureSheet_1.textures["Fire-1.png"]);
    // console.log(fire);
    let myEmitter = new particles.Emitter(particleContainer,
        textureSheet_1.textures["Fire-1.png"],
        // Emitter configuration, edit this to change the look
        // of the emitter
        {
            "alpha": {
                "start": 1,
                "end": 0
            },
            "scale": {
                "start": 0.04,
                "end": 1,
                "minimumScaleMultiplier": 1
            },
            "color": {
                "start": "#ff1717",
                "end": "#efff3d"
            },
            "speed": {
                "start": 100,
                "end": 100,
                "minimumSpeedMultiplier": 0.001
            },
            "acceleration": {
                "x": 0,
                "y": 0
            },
            "maxSpeed": 0,
            "startRotation": {
                "min": 260,
                "max": 280
            },
            "noRotation": false,
            "rotationSpeed": {
                "min": 1,
                "max": 10
            },
            "lifetime": {
                "min": 1,
                "max": 0.5
            },
            "blendMode": "add",
            "frequency": 0.05,
            "emitterLifetime": -1,
            "maxParticles": 10,
            "pos": {
                "x": 0,
                "y": 0
            },
            "addAtBack": false,
            "spawnType": "point"
        });

    myEmitter.emit = true;

    particleContainer.position.x = GAME_WIDTH / 2;
    particleContainer.position.y = GAME_HEIGHT / 2;

    app.stage.addChild(particleContainer);
    let elapsed = Date.now();

    const updateDescriptionText = () => {
        positionText.text = description + particleContainer.children.length;
    };

    app.ticker.add(() => {

        let now = Date.now();

        // The emitter requires the elapsed
        // number of seconds since the last update
        myEmitter.update((now - elapsed) * 0.001);
        elapsed = now;
        updateDescriptionText();
        stats.update();
    });
}


function onWindowResize() {
    // Determine which screen dimension is most constrained
    const ratio = Math.min(window.innerWidth / GAME_WIDTH, window.innerHeight / GAME_HEIGHT);
    // Scale the view appropriately to fill that dimension
    app.stage.scale.x = app.stage.scale.y = ratio;
    // Update the renderer dimensions
    app.renderer.resize(Math.ceil(GAME_WIDTH * ratio), Math.ceil(GAME_HEIGHT * ratio));
}

window.addEventListener('resize', onWindowResize, false);
