import * as PIXI from "pixi.js";
import Stats from 'stats.js';
import android = PIXI.utils.isMobile.android;

const FontFaceObserver = require('fontfaceobserver');

const GAME_WIDTH = 1280;
const GAME_HEIGHT = 720;


const app = new PIXI.Application({
    antialias: true,
    forceFXAA: true,
    resolution: 1,
    width: GAME_WIDTH,
    height: GAME_HEIGHT
});
app.renderer.autoResize = true;
app.renderer.view.style.position = "relative";
app.renderer.view.style.display = "block";
app.renderer.view.style.margin = "0 auto";

document.getElementById("webgl").appendChild(app.view);


// let background: any;
const headerFontStyle = {
    fontFamily: 'Arial',
    fontSize: 28,
    fill: 0x000000,
    align: 'center',
    dropShadow: true,
    dropShadowAngle: 1,
    dropShadowBlur: 0.2,
    dropShadowDistance: 0.2
};

const header = new PIXI.Text('SOFTGAMES DEMO - Random / Text & Images', headerFontStyle);
header.position.x = GAME_WIDTH / 2 - header.width / 2;
header.position.y = 20;

const pTxtFontStyle = {
    fontFamily: 'Arial',
    fontSize: 22,
    fill: 0x000000,
    align: 'left',
    dropShadow: true,
    dropShadowAngle: 1,
    dropShadowBlur: 0.2,
    dropShadowDistance: 0.2
};

const description = 'Spawn: ';
const descriptionTxt = new PIXI.Text(description, pTxtFontStyle);
descriptionTxt.position.x = GAME_WIDTH / 2 - descriptionTxt.width / 2;
descriptionTxt.position.y = header.position.y + header.height + 10;

const texts = ["WIN BIG", "FOLLOW US", "FREE COINS", "PLAY AGAIN", "YOU HAVE WON"];
// 💰 👍 🎮 💎 💲

let explanation = "Text phrases:\n";
for (let j = 0; j < texts.length; j++) {
    explanation += ("\n" + texts[j])
}
explanation += "\n\nImages:\n";

const copyFont = pTxtFontStyle;
copyFont.fontSize = 16;
const explanationTxt = new PIXI.Text(explanation, copyFont);
explanationTxt.position.x = 20;
explanationTxt.position.y = 80;


const TextureAtlas_01 = "../../texture-packed-assets/softgames.json";


let textFontStyle: any;
// @ts-ignore
const fontLoader = new FontFaceObserver('PermanentMarker');
fontLoader.load().then(function () {

    textFontStyle = {
        fontFamily: 'PermanentMarker',
        fontSize: 34,
        lineHeight: 60,
        fill: 0xFFD700,
        align: 'left',
        dropShadow: true,
        dropShadowAngle: 1,
        dropShadowBlur: 2,
        dropShadowDistance: 2,
        dropShadowColor: 0x000000
    };

    // @ts-ignore
    PIXI.Loader.shared.add(TextureAtlas_01).load(setup);
    onWindowResize();
});

const baseTextures: any = [];

const wrapper = new PIXI.Container();

function setup() {

    const stats = new Stats();
    document.body.appendChild(stats.dom);

    // @ts-ignore
    const textureSheet_1 = PIXI.Loader.shared.resources[TextureAtlas_01].spritesheet;
    const background = new PIXI.Sprite(textureSheet_1.textures["background.png"]);

    app.stage.addChild(background);
    app.stage.addChild(header, descriptionTxt, explanationTxt);
    app.stage.addChild(wrapper);

    baseTextures.push(textureSheet_1.textures["card.png"]);
    baseTextures.push(textureSheet_1.textures["joker.png"]);
    baseTextures.push(textureSheet_1.textures["explosion.png"]);
    baseTextures.push(textureSheet_1.textures["space-invaider.png"]);
    baseTextures.push(textureSheet_1.textures["alien.png"]);

    for(let i = 0; i < baseTextures.length; i++){
        const sprite = new PIXI.Sprite(baseTextures[i]);
        sprite.position.y = explanationTxt.position.y + explanationTxt.height - 40;
        sprite.position.x = 90 + (i*30);
        sprite.scale.x = 0.5;
        sprite.scale.y = 0.5;
        app.stage.addChild(sprite);
    }

    setInterval(spawn, 2500);
    // spawn();

    app.ticker.add(() => {
        stats.update();
    });
}


function spawn() {

    const objectIndexes = arrayOfRandomNumbers(4);
    // console.log(objectIndexes);

    const imgOrTxt1 = chooseImagesOrText(randomRange(0, 1));
    const imgOrTxt2 = chooseImagesOrText(randomRange(0, 1));
    const imgOrTxt3 = chooseImagesOrText(randomRange(0, 1));

    descriptionTxt.text = description + imgOrTxt1 + " + " + imgOrTxt2 + " + " + imgOrTxt3;
    descriptionTxt.position.x = GAME_WIDTH / 2 - descriptionTxt.width / 2;

    const obj1 = createObject(imgOrTxt1, objectIndexes[0]);
    const obj2 = createObject(imgOrTxt2, objectIndexes[1]);
    const obj3 = createObject(imgOrTxt3, objectIndexes[2]);

    wrapper.removeChildren();
    wrapper.addChild(obj1, obj2, obj3);

    obj1.position.x = 0;
    obj2.position.x = obj1.position.x + obj1.width + 20;
    obj3.position.x = obj2.position.x + obj2.width + 20;

    wrapper.position.x = GAME_WIDTH / 2 - wrapper.width / 2;

}

function chooseImagesOrText(index: number): string {
    if (index == 0) {
        return "txt"
    }
    return "img";
}


function createObject(txtOrImg: string, index: number) {

    if (txtOrImg === "txt") {
        const _fontSize = randomRange(24, 60);
        const colours = [0xFF0000, 0xFFD700, 0x000000];
        textFontStyle.fontSize = _fontSize;
        textFontStyle.lineHeight = _fontSize * 1.25;
        textFontStyle.fill = colours[randomRange(0, 2)];

        const text = new PIXI.Text(texts[index], textFontStyle);

        text.position.x = (GAME_WIDTH / 2) - text.height / 2;
        text.position.y = (GAME_HEIGHT / 2) - text.height / 2;

        return text;
    }

    const sprite = new PIXI.Sprite(baseTextures[index]);

    sprite.position.x = GAME_WIDTH / 2 - sprite.width / 2;
    sprite.position.y = GAME_HEIGHT / 2 - sprite.height / 2;


    return sprite;
}

function onWindowResize() {
    // Determine which screen dimension is most constrained
    const ratio = Math.min(window.innerWidth / GAME_WIDTH, window.innerHeight / GAME_HEIGHT);
    // Scale the view appropriately to fill that dimension
    app.stage.scale.x = app.stage.scale.y = ratio;
    // Update the renderer dimensions
    app.renderer.resize(Math.ceil(GAME_WIDTH * ratio), Math.ceil(GAME_HEIGHT * ratio));
}

window.addEventListener('resize', onWindowResize, false);


function randomRange(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function arrayOfRandomNumbers(max: number): number[] {
    const startR = Math.floor(Math.random() * max);
    let indexes = [startR];

    indexes = getRandomNumberNotInArray(indexes, max);
    indexes = getRandomNumberNotInArray(indexes, max);

    return indexes;
}

function getRandomNumberNotInArray(numbers: number[], max: number): number[] {
    const temp = Math.floor(Math.random() * max);

    if (numbers.includes(temp)) {
        return getRandomNumberNotInArray(numbers, max);
    }

    numbers.push(temp);
    return numbers;
}
