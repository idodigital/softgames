declare module '*.png'
declare module '*.jpg'
declare module "*.jpg" {
    const value: any;
    export default value;
}
declare module '*.gif'
declare module  '*.css'
declare  module  '*.js'
declare module '*.scss' {
    const styles: { [className: string]: string };
    export default styles;
}
declare module '*.json'

// Note: Must also include the pixi.js typings globally!
/// <reference path="node_modules/pixi-particles/ambient.d.ts" />