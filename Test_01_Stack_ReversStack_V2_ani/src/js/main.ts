import * as PIXI from "pixi.js";
// @ts-ignore
import {TimelineMax, Power4} from "gsap/all";
// @ts-ignore
import * as dat from 'dat.gui';

import Stats from 'stats.js';

const gui = new dat.GUI();

let animationTime = {'animation speed': 1};
let delayTime = {'delay speed': 1};

const speedFolder = gui.addFolder("Adjust speeds");
speedFolder.add(animationTime, "animation speed", 0.05, 2);
speedFolder.add(delayTime, "delay speed", 0.05, 2);

const GAME_WIDTH = 1280;
const GAME_HEIGHT = 720;

const TextureAtlas_01 = "../../texture-packer-assets/texture.json";

const CardsSpriteMainContainer: any = [];

const app = new PIXI.Application({
    antialias: true,
    forceFXAA: true,
    resolution: 1,
    width: GAME_WIDTH,
    height: GAME_HEIGHT
});
app.renderer.autoResize = true;
app.renderer.view.style.position = "relative";
app.renderer.view.style.display = "block";
app.renderer.view.style.margin = "0 auto";

document.getElementById("webgl").appendChild(app.view);

// @ts-ignore
PIXI.Loader.shared.add(TextureAtlas_01).load(setup);

const positions: any = [];

let background: any;
const headerFontStyle = {
    fontFamily: 'Arial',
    fontSize: 28,
    fill: 0xFFFFFF,
    align: 'center',
    dropShadow: true,
    dropShadowAngle: 1,
    dropShadowBlur: 1,
    dropShadowDistance: 1
};

const textFontStyle = {
    fontFamily: 'Arial',
    fontSize: 24,
    fill: 0xFFFFFF,
    align: 'center',
    dropShadow: true,
    dropShadowAngle: 1,
    dropShadowBlur: 1,
    dropShadowDistance: 1
};

const header = new PIXI.Text('SOFTGAMES DEMO - Stack / Reverse Stack V2', headerFontStyle);
header.position.x = GAME_WIDTH / 2 - header.width / 2;
header.position.y = 40;

let spriteNumber = 0;
const description = 'Movie Clips Number in Animation: ';

const positionText = new PIXI.Text(description + spriteNumber, textFontStyle);
positionText.position.x = GAME_WIDTH / 2 - positionText.width / 2;
positionText.position.y = header.position.y + header.height + 5;

const cardContainer = new PIXI.Container();
const CardsSpriteMainContainer: any = [];

onWindowResize();

function setup() {

    const stats = new Stats();
    document.body.appendChild(stats.dom);

    // @ts-ignore
    const textureSheet_1 = PIXI.Loader.shared.resources[TextureAtlas_01].spritesheet;

    let Clubs_Queen_frames = [];
    let Diamonds_Queen_frames = [];

    for (var i = 0; i < 43; i++) {
        var val = i < 10 ? '0' + i : i;

        Clubs_Queen_frames.push(PIXI.Texture.from('Clubs/queen_clubs_000' + val + '.png'));
        Diamonds_Queen_frames.push(PIXI.Texture.from('Diamonds/queen_diamonds_000' + val + '.png'));
    }


    background = new PIXI.Sprite(textureSheet_1.textures["assets/background2.png"]);
    background.width = GAME_WIDTH;
    background.height = GAME_HEIGHT;

    app.stage.addChild(background);
    app.stage.addChild(header, positionText);
    app.stage.addChild(cardContainer);

    addCard_SpritesToContainer(Clubs_Queen_frames, Diamonds_Queen_frames);


    addCardsToStage();
    startAnimation();


    app.ticker.add(() => {
        stats.update();

    });
}


function onWindowResize() {
    // Determine which screen dimension is most constrained
    const ratio = Math.min(window.innerWidth / GAME_WIDTH, window.innerHeight / GAME_HEIGHT);
    // Scale the view appropriately to fill that dimension
    app.stage.scale.x = app.stage.scale.y = ratio;
    // Update the renderer dimensions
    app.renderer.resize(Math.ceil(GAME_WIDTH * ratio), Math.ceil(GAME_HEIGHT * ratio));
}

window.addEventListener('resize', onWindowResize, false);

function addCard_SpritesToContainer(Clubs_Queen_frames: any, Diamonds_Queen_frames: any) {

    for (let i = 0; i < 72; i++) {

        const Clubs_Queen_anim = new PIXI.AnimatedSprite(Clubs_Queen_frames);
        Clubs_Queen_anim.x = GAME_WIDTH / 2;
        Clubs_Queen_anim.y = GAME_HEIGHT / 2;
        Clubs_Queen_anim.anchor.set(0.5);
        Clubs_Queen_anim.animationSpeed = 0.5;
        Clubs_Queen_anim.play();

        const Diamonds_Queen_anim = new PIXI.AnimatedSprite(Diamonds_Queen_frames);
        Diamonds_Queen_anim.x = GAME_WIDTH / 2 + 150;
        Diamonds_Queen_anim.y = GAME_HEIGHT / 2;
        Diamonds_Queen_anim.anchor.set(0.5);
        Diamonds_Queen_anim.animationSpeed = 0.5;
        Diamonds_Queen_anim.play();

        CardsSpriteMainContainer.push(Clubs_Queen_anim);
        CardsSpriteMainContainer.push(Diamonds_Queen_anim);
    }
}

function addCardsToStage() {

    for (let i = 0; i < CardsSpriteMainContainer.length; i++) {

        CardsSpriteMainContainer[i].scale.x = CardsSpriteMainContainer[i].scale.y = 0.3;

        const _y = GAME_HEIGHT/2 - (i * 1);
        CardsSpriteMainContainer[i].position.y = _y;

        const _x = 30 + (i * 8);
        CardsSpriteMainContainer[i].position.x = _x;

        positions.push({x: _x, y: _y});

        cardContainer.addChild(CardsSpriteMainContainer[i]);


        positionText.text = description + spriteNumber;
        spriteNumber++;
    }

}


function startAnimation() {

    // @ts-ignore
    // const delayTask = new Task();
    // const delayTimeline = new TimelineMax();
    // delayTask.execute = ()=>{
    // delayTimeline.to({time:0}, 2, {time:1}).pause().restart().play();
    // };


    // @ts-ignore
    const StarterQueue = new SequentialQueue();
    StarterQueue.name = "The Start Queue.";
    // StarterQueue.add(delayTask);
    // @ts-ignore
    const ReverseQueue = new SequentialQueue();
    ReverseQueue.name = "The Reverse Queue.";

    let j = 0;
    let k = 0;


    for (let i = CardsSpriteMainContainer.length - 1; i > -1; i--) {

        const __x = positions[j].x;
        const __y = positions[j].y;
        // @ts-ignore
        const T = new Task();
        T.execute = () => {

            positionText.text = description + spriteNumber;
            spriteNumber--;

            const timeline = new TimelineMax();
            const spriteToAnimate = CardsSpriteMainContainer[i];
            let flag = false;
            timeline.to(spriteToAnimate.position, animationTime['animation speed'], {
                x: __x,
                y: __y + 250,
                ease: Power4.easeIn,
                delay: delayTime["delay speed"],
                onComplete: () => {
                    // cardContainer.setChildIndex(spriteToAnimate, j);
                    T.notifyComplete();
                },
                onUpdate: () => {
                    if (timeline.progress() >= 0.5 && !flag) {
                        flag = true;
                        cardContainer.setChildIndex(spriteToAnimate, cardContainer.children.length - 1);
                    }
                }
            });
            timeline.pause();
            timeline.restart();
            timeline.play();
        };
        StarterQueue.add(T);
        j++;

        // @ts-ignore
        const RT = new Task();
        RT.execute = () => {
            positionText.text = description + spriteNumber;
            spriteNumber++;

            const timeline = new TimelineMax();
            if (k === 144) {
                k = 0
            }
            const spriteToAnimate = CardsSpriteMainContainer[k];
            let flag = false;

            timeline.to(spriteToAnimate.position, animationTime['animation speed'], {
                x: __x,
                y: __y,
                ease: Power4.easeIn,
                delay: delayTime["delay speed"],
                onComplete: () => {
                    // cardContainer.setChildIndex(spriteToAnimate, cardContainer.children.length - 1);
                    RT.notifyComplete();
                },
                onUpdate: () => {
                    if (timeline.progress() >= 0.5 && !flag) {
                        flag = true;
                        cardContainer.setChildIndex(spriteToAnimate, cardContainer.children.length - 1);
                    }
                }
            });
            timeline.pause();
            timeline.restart();
            timeline.play();
            k++;
        };


        ReverseQueue.add(RT);
    }


    // StarterQueue.notifyComplete = function () {
    //     ReverseQueue.execute();
    // };


    // StarterQueue.execute();
    // @ts-ignore
    const mainLoopingQueue = new LoopingQueue();
    mainLoopingQueue.add(StarterQueue);
    mainLoopingQueue.add(ReverseQueue);
    mainLoopingQueue.execute();
}

function Task() {
    const self = this;
    self.notifyComplete = function () {
    };
    self.execute = function () {
    }
}

function SequentialQueue() {
    const self = this;

    self.name = "Base Queue";
    const queue: any = [];
    let playIndex = 0;

    self.notifyComplete = function () {
    };

    self.remove = function () {
    };

    self.add = function (task: any) {
        if (typeof task.execute !== "function") {
            return console.warn("Only add tasks to tasks queues!.")
        }

        task.notifyComplete = finished;

        queue.push(task);
    };

    self.execute = function () {
        if (queue.length <= 0) {
            return console.warn("Can't execute an empty queue");
        }

        // console.log(typeof queue[playIndex].execute);
        // console.log("------");
        queue[playIndex].execute();
    };


    const finished = function () {
        playIndex++;

        if (playIndex === queue.length) {
            playIndex = 0;
            self.notifyComplete();
            console.log(self.name + " is over.");
            return
        }

        self.execute();
    };

}


function LoopingQueue() {
    const self = this;

    const queue: any = [];
    let playIndex = 0;

    self.add = function (queueToAdd: any) {
        if (typeof queueToAdd.execute !== "function" && queueToAdd._type !== "BaseTask") {
            return console.warn("You can only had BaseTask's to LoopingQueues.")
        }

        queueToAdd.notifyComplete = finished;

        queue.push(queueToAdd);
    };

    self.remove = function () {

    };

    self.execute = function () {
        if (queue.length <= 0) {
            return console.warn("Can't execute an empty queue");
        }

        queue[playIndex].execute();
    };

    const finished = function () {
        playIndex++;

        if (playIndex === queue.length) {
            playIndex = 0;
        }

        self.execute();
    };

    self.print = function () {
        console.log("Queue: ", queue);
    };

};